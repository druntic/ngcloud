const FileService = require('../services/file.service');

const mongoose = require("mongoose");
const objectId = mongoose.Types.ObjectId;
const Readable = require("stream").Readable;


let bucket;
mongoose.connection.on("connected", () => {
    bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
});

class FileController {
    static async upload(req, res) {
        let file = req.files.file;
        let contentType = file.mimetype;
        let filename = file.name;
        let owner = objectId(req.payload._id);
        let metadata = { owner };

        const readableStream = new Readable();
        readableStream.push(file.data);
        readableStream.push(null);

        let uploadStream = bucket.openUploadStream(filename, { contentType, metadata });
        readableStream.pipe(uploadStream);

        let message;
        uploadStream.on("error", () => {
            message = "Error when uploading file";
            return res.status(500).json({ message });
        });

        uploadStream.on("finish", async () => {
            try {
                message = "File uploaded";
                let fileInfo = await FileService.findFileById(uploadStream.id);
                return res.status(201).json({ message, fileInfo });
            } catch(err) {
                return res.status(500).json(err);
            }
        });
    }

    static async getFiles(req, res) {
        let length;
        let files;

        try {
            files = await FileService.findFileByOwnerId(req.payload._id);
            length=await FileService.countFiles(req.payload._id);
        } catch(err) {
            return res.status(500).json(err);
        }
        let message = "Sending files";
        return res.status(200).send({ files, message, length });
    }

    static async download(req, res) {
        
        let id = req.params.id;
        let file, message;
        //let length;
       
        try {
            file = await FileService.findFileById(id);
            //length = await File.countFiles(req.payload._id);
        } catch(err) {
            return res.status(500).json(err);
        }

        if (!file) {
            message = "Not found";
            return res.status(404).json({message});
        }

        let downloadStream = bucket.openDownloadStream(objectId(id));

        res.writeHead(200,{"content-type": file.contentType});
        downloadStream.pipe(res);
    }

    static async delete (req, res) {
        let id = req.params.id;
        let message, file;
        
        try {
            file = await FileService.deleteFileById(id);
        } catch(err) {
            return res.status(500).json({ err });
        }

        if (!file) {
            message = "File not found"
            return res.status(500).json(err);
        }

        message = "Successfully removed!";
        return res.status(200).json({message});
    }
    static async getFiles(req, res) {
        let files;
        let length;
        let pagdata = JSON.parse(req.headers['pagdata']);
        let size = pagdata['size'];
        let page = pagdata['page'];
        let sort = pagdata['sort'];
        let query = {"skip": size * (page - 1), "limit": size};
        let pagination = {query, sort}
        
        console.log(pagination);
        
        try
         {
            files = await FileService.findFileByOwnerId(req.payload._id, pagination);
            
            
        } catch(err) {
            return res.status(500).json(err);
        }
 
        let message = "Sending files";
        return res.status(200).send({ files, message,length });
    }



}

module.exports = FileController;