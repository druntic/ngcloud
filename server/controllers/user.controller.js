const UserService = require("../services/user.service");


class UserController {
    static async register(req, res) {
        let username = req.body.username;
        let email = req.body.email;
        let password = req.body.password;

        let user, token, message;

        try {
            user = await UserService.createUser(username, email, password);
        } catch(err) {
            return res.status(500).json(err);
        }

        message = "Successfully registered";
        token = user.generateJWT();

        return res.status(201).json({ message, token })
    }

    static async login(req, res) {
         let email = req.body.email;
         let password = req.body.password;

         let user, message, token;

         try {
            user = await UserService.getUserByEmail(email);
         } catch(err) {
             return res.status(500).json(err);
         }

         if (!user || !user.validPassword(password)) {
            message = "Incorrect email or password";
            return res.status(404).json({ message });
        }
        
        token = user.generateJWT();
        message = "Successfully logged in!";
        return res.status(202).json({ token, message });
    }
}


module.exports = UserController;