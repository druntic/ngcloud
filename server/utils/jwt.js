const jwt = require("express-jwt");

const secret = "MY_SECRET";
const userProperty = "payload";

const authMiddleware = jwt({ secret, userProperty });




module.exports = { authMiddleware, secret };