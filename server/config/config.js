const port = 3000;
const host = "localhost";
const root = "../client/dist/ngcloud";
const dbURI = "mongodb://localhost:27017/ngcloud";

module.exports = { port, host, root, dbURI }