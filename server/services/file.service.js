const mongoose = require("mongoose");
const File = mongoose.model("File");
const Chunk = mongoose.model("Chunk");

const objectId = mongoose.Types.ObjectId;

class FileService {
    static async findFileById(id) {
        let _id = objectId(id);
        return await File.findOne({ _id }).exec();
    }
    static async findFileByOwnerId(id){
        return await File.find({ "metadata.owner": id }).exec();
    }
    static async deleteFileById(id) {
        await Chunk.deleteMany({ files_id: id }).exec();
        return await File.deleteOne({ _id: id }).exec();
    }
    static async findFileByOwnerId(id, pagination = {}){
        
        if (pagination == {}) {
            return await File.find({ "metadata.owner": id }).exec();
        }
        else {
            switch(pagination["sort"]) {
              case 1:
                return await File.find({ "metadata.owner": id },{},pagination["query"]).sort({ filename: 0 }).exec();
                break;
              case 2:
                return await File.find({ "metadata.owner": id },{},pagination["query"]).sort({ uploadDate: 0 }).exec();
                break;
              case 3:
                return await File.find({ "metadata.owner": id },{},pagination["query"]).sort({ length: 0 }).exec();
                break;
              case -1:
                return await File.find({ "metadata.owner": id },{},pagination["query"]).sort({ filename: -1 }).exec();
                break;
              case -2:
                return await File.find({ "metadata.owner": id },{},pagination["query"]).sort({ uploadDate: -1 }).exec();
                break;
              case -3:
                return await File.find({ "metadata.owner": id },{},pagination["query"]).sort({ length: -1 }).exec();
                break;
              default:
                return await File.find({ "metadata.owner": id },{},query).exec();
                break;
            }
        }
    }
    static async countFiles(id) {
        
        return await File.countDocuments({ "metadata.owner": id }).exec();
        
    }
}

module.exports = FileService;