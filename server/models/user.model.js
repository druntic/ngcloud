const mongoose = require("mongoose");
const crypto = require("crypto");
const jsonwebtoken = require("jsonwebtoken");
const gravatar = require("gravatar");

const mySecret = require("../utils/jwt").secret;

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    username: {
        type: String,
        required: true
    },
    gravatarURL: {
        type: String,
        required: true
    },
    hash: String,
    salt: String
});

userSchema.methods.setPassword = function(plain) {
    this.salt = crypto.randomBytes(256).toString("hex");
    this.hash = crypto.pbkdf2Sync(plain, this.salt, 1000, 64, "sha512");
}

userSchema.methods.validPassword = function(plain) {
    let hash = crypto.pbkdf2Sync(plain, this.salt, 1000, 64, "sha512");
    return hash == this.hash;
}

userSchema.methods.setGravatar = function(email) {
    this.gravatarURL = gravatar.url(email);
}

userSchema.methods.generateJWT = function() {
    let expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    
    return jsonwebtoken.sign({
        _id: this._id,
        email: this.email,
        username: this.username,
        gravatarURL: this.gravatarURL,
        exp: Math.trunc(expiry.getTime() / 1000) 
    }, mySecret);
}

mongoose.model("User", userSchema);