const express = require("express");
let router = express.Router();

const userRoutes = require("./user.routes");
const fileRoutes = require("./file.routes");

router.use("/user", userRoutes);
router.use("/files", fileRoutes); // mozda files

// router.use("/notifications", notificationRoutes);

module.exports = router;