const UserController = require("../controllers/user.controller");

const express = require("express");
let router = express.Router();

router.route("/")
    .post(UserController.register);

router.route("/login")
    .post(UserController.login);

module.exports = router;


