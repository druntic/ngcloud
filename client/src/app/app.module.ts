import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { CollapseModule } from 'ngx-bootstrap';
import { FilesComponent } from './components/files/files.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RemoveHostDirective } from './directives/remove-host.directive';
import { AccountComponent } from './components/account/account.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { AppRouter } from './app.routes';
import { ImagePathPipe } from './pipes/image-path.pipe';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';
import { UploadModalComponent } from './upload-modal/upload-modal.component';


import { ModalModule } from 'ngx-bootstrap/modal';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    FilesComponent,
    NavbarComponent,
    RemoveHostDirective,
    AccountComponent,
    LoginComponent,
    RegisterComponent,
    ImagePathPipe,
    UploadModalComponent
  ],
  imports: [
    BrowserModule,
    CollapseModule.forRoot(),
    AppRouter,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added,
    ProgressbarModule.forRoot(),
    ModalModule.forRoot(),
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    UploadModalComponent
  ]
})
export class AppModule { }
