import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpEventType, HttpRequest } from '@angular/common/http';
import { UserService } from './services/user.service';

import { throwError, Observable, BehaviorSubject } from 'rxjs';
import { map, tap, last, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FileMetadata } from './classes/file-metadata';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  public files: BehaviorSubject<FileMetadata[]> = new BehaviorSubject<FileMetadata[]>([]);
  public length: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  fileUrl: string = "/api/files";
  
  constructor(
    public httpClient: HttpClient, 
    public userService: UserService
    // public modal: BsModalRef
  ) { 
    this.userService.loggedIn.subscribe((loggedIn: boolean) => {
      if(loggedIn) 
        this.updateSelfFiles();
      else {console.log("Unauthorised"); this.files.next([])};
    });
    
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');

    
  };

  uploadStatus(event: any): { percentage: number, done: boolean } {
    if(event.body) {
      
      this.updateSelfFiles();
    }
    
    let percentage = Math.round(100 * event.loaded / event.total);
    let done = HttpEventType.Response == event.type;

    return { percentage, done };
  }

  upload(file: any, dataHandler: (percentage: number, done: boolean) => any) {
    let formData: FormData = new FormData();
    formData.append('file', file, file.name);

    let headers = this.userService.authHeaders();

    const req = new HttpRequest(
      'POST',
      this.fileUrl,
      formData,
      { reportProgress: true, headers }
    );

    return this.httpClient.request(req)
      .pipe(
        map(event => this.uploadStatus(event)),
        tap((status: { percentage: number, done: boolean }) => dataHandler(status.percentage, status.done)),
        last(),
        catchError(this.handleError)
      );
  }

   // CLIENT file.service.ts
   public page_size: number = 5;
   public my_page: number = 1;
   public my_sort: number = 3; // (1,2,3) or (-1,-2,-3) == (filename, uploadDate, length)
   public my_pages: number = 10;
   public list:number[] = [1,2,3,4,5]; 
   public nameArrow=0;
   public sizeArrow=0;
   public dataArrow=0;

   getFiles(): Observable<{ files: FileMetadata[], message: string, length: number }> {
    
    let headers = this.userService.authHeaders();
    headers = headers.append('pagdata', JSON.stringify({ "size": this.page_size, "page": this.my_page, "sort": this.my_sort }));
    return this.httpClient.get<{ files: FileMetadata[], message: string, length: number }>(this.fileUrl, {headers});
    
}
updateSelfFiles() {
  
  this.getFiles().subscribe( data => { this.files.next(data.files);this.length.next(data.length); }, err => this.handleError(err));
}

delete(file: FileMetadata): any {
  let headers = this.userService.authHeaders();
  return this.httpClient.delete(`${this.fileUrl}/${file._id}`,{ headers })
    .pipe(
      tap(event => {

        this.updateSelfFiles();
      }),
      catchError(this.handleError)
    );
}}
