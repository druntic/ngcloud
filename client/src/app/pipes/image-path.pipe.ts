import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imagePath'
})
export class ImagePathPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    let extension = value.filename.split(".").pop();
    let availableExtensions = ["doc", "docx", "jpg", "mp3", "pdf", "ppt", "pptx", "xls", "xlsx", "txt"];
    if(!availableExtensions.includes(extension.toLowerCase()))
        extension = "file";

    return `../assets/images/${extension.toLowerCase()}.png`;
  }

}
