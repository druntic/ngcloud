export class FileMetadata {
    constructor(
        public _id: string,
        public chunkSize: number,
        public contentType: string,
        public filename: string,
        public length: number,
        public md5: string,
        public metadata: { owner: string },
        public uploadDate: string
    ) {}


    setImage(): string {

        let extension = this.filename.split(".").pop();
        let availableExtensions = ["doc", "docx", "jpg", "mp3", "pdf", "ppt", "pptx", "xls", "xlsx", "txt"];
        if(!availableExtensions.includes(extension.toLowerCase()))
            extension = "file";

        return `../assets/images/${extension.toLowerCase()}.png`;
    }
}
