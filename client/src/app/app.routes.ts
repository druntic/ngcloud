import { Routes, RouterModule, Router } from '@angular/router';
import { FilesComponent } from './components/files/files.component';
import { AccountComponent } from './components/account/account.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
    { path: "", redirectTo: "files", pathMatch: "full" },
    { path: "files", component: FilesComponent },
    { path: "account", component: AccountComponent },
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "**", redirectTo: "files" }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRouter {};