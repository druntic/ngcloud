import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Token } from '../classes/token';
import { Router } from '@angular/router';

export type User = { username: string, email: string, gravatarURL: string, _id: any };
export type Credentials = { username?: string, email: string, password: string };

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  private url: string = "/api/user";

  public user: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean> (false);

  constructor(public http: HttpClient, public router: Router) {
    if (this.isLoggedIn())
      this.user.next(this.credentials());
  }

  isLoggedIn() {
    let token = Token.get();
    if (token) {
      let payload = Token.parse(token);
      this.loggedIn.next(payload.exp > Date.now() / 1000);
      return payload.exp > Date.now() / 1000;
    } else return false;
  }

  credentials(): User {
    if (this.isLoggedIn()) {
      let token = Token.get();
      let payload = Token.parse(token);
      delete payload["iat"];
      delete payload["exp"];
      return payload;
    } return null;
  }

  saveToken(token: string) {
    Token.save(token);
    this.user.next(this.credentials());
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  register(credentials: Credentials): Observable<any> {
    return this.http.post(this.url, credentials)
      .pipe(
        tap(
          (response: { message: string, token: string }) => {
            this.saveToken(response.token);
            this.router.navigate(['files']);
            this.loggedIn.next(true);
          }
        ),
        catchError(this.handleError)
      );
  }

  login(credentials: Credentials): Observable<any> {
    return this.http.post(this.url + "/login", credentials)
      .pipe(
        tap(
          (response: { message: string, token?: string }) => {
            if (response["token"]) {
              this.saveToken(response.token);
              this.router.navigate(['files']);
              this.loggedIn.next(true);
            }
          }
        ),
        catchError(this.handleError)
      );
  }

  logout(): void {
    Token.remove();
    this.router.navigate(["/login"]);
    this.user.next(null);
    this.loggedIn.next(false);
  }

  authHeaders() {
    return new HttpHeaders({
      'Authorization': `Bearer ${Token.get() ? Token.get() : ""}`
    });
  }


}
