import { Component, OnInit } from '@angular/core';
import { User, UserService } from '../../services/user.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public userService: UserService) { }

  user: User;
  ngOnInit() {
    this.userService.user.subscribe((user: User) => {
      this.user = user;
    });
  }

}
