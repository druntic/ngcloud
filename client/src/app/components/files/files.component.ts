import { Component, OnInit } from '@angular/core';

import { FileMetadata } from '../../classes/file-metadata';
import { from } from 'rxjs';

import { BsModalService } from 'ngx-bootstrap/modal';
import { UploadModalComponent } from '../../upload-modal/upload-modal.component';

import { FileService } from '../../file.service'
import { ActivatedRoute, Router } from '@angular/router';
import { SortableComponent } from 'ngx-bootstrap';
@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {
  config: any;
  private files: FileMetadata[] = [];
  

  
  constructor(public modalService: BsModalService, public fileService: FileService,private route: ActivatedRoute, private router: Router) {
    this.config = {
      currentPage: 1,
      itemsPerPage: 5,
      totalItems:0
      };
      route.queryParams.subscribe(
      params => this.config.currentPage= params['page']?params['page']:1 );
   }

  ngOnInit() {
    this.fileService.files.subscribe((files: FileMetadata[]) => {
      this.files = files;
    });
    this.fileService.length.subscribe((length: number) => {
      let newlength=length;
      console.log(newlength);
    });
    
  }

  openModal() {
    this.modalService.show(UploadModalComponent);
  }
  pageChange(newPage: number) {
    this.router.navigate([''], { queryParams: { page: newPage } });
  }

  public nameCounter=1;
  public sizeCounter=3;
  public dateCounter=2;

  sort1(){
    
    this.fileService.nameArrow=1;
    this.fileService.sizeArrow=0;
    this.fileService.dataArrow=0; 
      this.fileService.my_sort=this.nameCounter;
      this.fileService.updateSelfFiles(); 
      this.nameCounter=-this.fileService.my_sort;
       
  }
  sort2(){
    this.fileService.nameArrow=0;
    this.fileService.sizeArrow=0;
    this.fileService.dataArrow=1;  
    this.fileService.my_sort=this.dateCounter;
    this.fileService.updateSelfFiles(); 
    this.dateCounter=-this.fileService.my_sort;
        
}
sort3(){
  this.fileService.nameArrow=0;
  this.fileService.sizeArrow=1;
  this.fileService.dataArrow=0; 
  this.fileService.my_sort=this.sizeCounter;
  this.fileService.updateSelfFiles(); 
  this.sizeCounter=-this.fileService.my_sort;
  
        
} 

prevPage(){
  if(this.fileService.my_page>1){
    this.fileService.my_page--;
    this.fileService.updateSelfFiles(); 
  }
}
nextPage(){
  if(this.fileService.my_page<this.fileService.list.length){
    this.fileService.my_page++;
    this.fileService.updateSelfFiles(); 
  }
}
pPage(page: number){
  this.fileService.my_page=page;
  this.fileService.updateSelfFiles();
}
}


